from random import randint
from turtle import position


class Monde:
    def __init__(self,dimension, duree_repousse):
        self.dimension = dimension
        self.__duree_repousse = duree_repousse
        self.carte = [[] for i in range(self.dimension)]
        for k in self.carte:
            for j in range(dimension):
                herbe_ou_pas = randint(1,2) #1 chance sur 2 que la case soit herbue au départ
                if herbe_ou_pas == 1: #1 correspond à mettre de l'herbe
                    k.append([-1]) #CASE SOUS FORME D'UNE LISTE POUR ACCUEILLIR VALEUR ET MOUTONS / LOUPS...
                else: #ajoute entier aléatoire entre 0 et duree_repousse
                    k.append([randint(0,self.__duree_repousse)])

    def herbePousse(self):
        for j in self.carte:
            for i in j:
                if i[0] >= self.__duree_repousse:
                    i[0] = -1 # -1 --> une case d'herbe 
                elif i[0] < self.__duree_repousse and i[0] != -1:
                    i[0] += 1
    
    def herbeMangee(self,i,j):
        self.carte[i][j][0] = 0

    def nbHerbe(self) -> int: #renvoie un ENTIER
        compte = 0
        for i in self.carte:
            for j in i:
                if j[0] == -1:
                    compte += 1
        return compte

    def CoefCarte(self, i, j):
        return self.carte[i][j]

    def getCarte(self):
        return self.carte

