from random import randint
import random
import re

class Loup:
    def __init__(self, position_x, position_y, carte, numLoup):
        self.position_x = position_x
        self.position_y = position_y
        self.gain_nourriture = 18 #entier : gain d'énergie par consommation d'un mouton
        self.taux_reproduction = 3 #entier : pourcentage de chance de reproduction 
        self.carte = carte #classe Monde
        self.energie = randint(1, 2*self.gain_nourriture)
        self.numLoup = numLoup
    def __repr__(self):
        return 'Loup, x :'  + str(self.position_x) + ', y :' + str(self.position_y)

    '''recherche de Mouton dans la case'''
    def variationEnergie(self):
        pattern = r'^Mouton'
        for i in self.carte[self.position_y][self.position_x]:
            if re.match(pattern, str(i)):
                self.energie += self.gain_nourriture
                break
        if self.energie != 0:
            self.energie -= 1

    def deplacement(self):
        var_ligne = randint(-1,1)
        if var_ligne == 0:
            var_col = random.choice([-1,1])
        else:
            var_col = randint(-1,1)
        self.position_x = (self.position_x + var_col + len(self.carte)) % len(self.carte) #colonnes numérotées de 0 à dimension - 1
        self.position_y = (self.position_y + var_ligne + len(self.carte)) % len(self.carte) #lignes numérotées de 0 à dimension - 1
        
    
    def place_loup(self):
        try:
            self.carte[self.position_y][self.position_x].append('Loup'+str(self.numLoup))
        except:
            print('error')
    
    def trouver_mouton(self):
        '''Post: int numero loup  ou booléen False'''
        for j in range(self.position_y-1, self.position_y+2):
            for k in range(self.position_x-1, self.position_x+2):
                if j != self.position_y and k != self.position_x: #Exclut la case où se trouve le chasseur, donc 8 cases au total
                    pattern = r'^Mouton'
                    ''' C'est un monde torique, donc on peut ressortir par le bas ou le haut si on dépasse'''
                    j = (j + len(self.carte)) % len(self.carte) #ligne
                    k = (k + len(self.carte)) % len(self.carte) #colonne
                    for item in self.carte[j][k]:
                        if re.match(pattern, str(item)):
                            return int(item[6:])
        return False                    
