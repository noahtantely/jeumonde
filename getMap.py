import pygame as p
import re
from getGraphe import var 

'''
FICHIER DEDIE A L'AFFICHAGE DE L'UI
'''

WIDTH = HEIGHT = 1000
DIMENSION = len(var[-1])
SQ_SIZE = HEIGHT // DIMENSION
MAX_FPS = 15
IMAGES = {}



class GameState():
    def __init__(self):
        self.board = var[-1]

def loadImages():
    IMAGES['wolf'] = p.transform.scale(p.image.load('images/wolf.jpg'), (SQ_SIZE, SQ_SIZE))
    IMAGES['wolf'].set_colorkey((255,255,255)) #transparence 100%
    IMAGES['sheep'] = p.transform.scale(p.image.load('images/sheep.png'), (SQ_SIZE, SQ_SIZE))
    IMAGES['sheep'].set_colorkey((255,255,255))
    IMAGES['rifle'] = p.transform.scale(p.image.load('images/rifle.jpg'), (SQ_SIZE, SQ_SIZE))
    IMAGES['rifle'].set_colorkey((255,255,255))

def main():
    '''
    Fonction principale appelant toutes les autres fonctions
    ''' 
    p.init()
    screen = p.display.set_mode((WIDTH, HEIGHT))
    p.display.set_caption('AFFICHAGE CARTE ')
    clock = p.time.Clock()
    screen.fill(p.Color("white"))
    gamestate = GameState()
    loadImages()
    running  = True
    while running:
        for e in p.event.get():
            if e.type == p.QUIT: #Fermeture de la fenêtre par la croix en haut à droite
                running = False
        drawGameState(screen, gamestate)
        clock.tick(MAX_FPS)
        p.display.flip()

def drawGameState(screen, gs):
    drawBoard(screen, gs.board)
    drawPieces(screen, gs.board)


def drawBoard(screen, map):
    '''
    Dessine le plateau
    '''
    for r in range(DIMENSION):
        for c in range(DIMENSION):
            if map[r][c][0] == -1: #case herbue
                p.draw.rect(screen, p.Color("dark green"), p.Rect(c*SQ_SIZE, r*SQ_SIZE, SQ_SIZE, SQ_SIZE))
            else:
                p.draw.rect(screen, p.Color("light grey"), p.Rect(c*SQ_SIZE, r*SQ_SIZE, SQ_SIZE, SQ_SIZE))


def drawPieces(screen, board):
    '''
    Place les icones sur les cases
    '''
    for r in range(DIMENSION):
        for c in range(DIMENSION):
            for element in board[r][c]:
                case = str(element)
                if re.match(r'^Mouton', case):
                    screen.blit(IMAGES['sheep'], p.Rect(c*SQ_SIZE, r*SQ_SIZE, SQ_SIZE, SQ_SIZE))
                if re.match(r'^Loup', case):
                    screen.blit(IMAGES['wolf'], p.Rect(c*SQ_SIZE, r*SQ_SIZE, SQ_SIZE, SQ_SIZE))
                if re.match(r'^Chasseur', case):
                    screen.blit(IMAGES['rifle'], p.Rect(c*SQ_SIZE, r*SQ_SIZE, SQ_SIZE, SQ_SIZE))
                    
'''
Si on exécute le fichier, lance la fonction main
'''                   
if __name__ == "__main__":
    main()

