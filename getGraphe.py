'''IMPORT LIBRAIRIES'''

import matplotlib.pyplot as plt

'IMPORT CLASSES'

from classeMonde import Monde
from classeSimulation import Simulation



monde1 = Monde(50, 30) #dimension, durée_repousse
sim1 = Simulation(200,600,100,monde1) #nombre_moutons, nombre_moutons_max, fin_du_monde, objet monde
var = sim1.simMouton()

def affiche_graphe(var):
    y1 = var[0]
    y2 = var[1]
    y3 = var[2]

    plt.plot([i for i in range(var[3])],y1,label = 'resultat_herbes',color='green')
    plt.plot([i for i in range(var[3])], y2, label = 'resultat_moutons', color='grey')
    plt.plot([i for i in range(var[3])], y3, label = 'resultat_loups', color='orange')
    plt.xlabel("Horloge")
    plt.legend()
    plt.show()

''' Moyen de n'executer que si le fichier est run  '''
if __name__ == '__main__':
    affiche_graphe(var) 