
PREREQUIS : 

    -Cloner le dossier Gitlab sur votre pc, Ouvrir dans un IDE Python
    -Installer la librairie pygame en exécutant "pip install pygame" dans un Terminal



'''
HOW TO COMMIT / PUSH TO GITLAB REPOSITORY
'''

Run in a terminal where your repository is cloned the following lines
==> git pull
==> git add --all
==> git commit -m {your message}
==> git remote set-url origin https://{userName}:{password}@gitlab.com/noahtantely/jeumonde.git
==> git push





#DESCRIPTION DU JEU

'''
Univers composé d'objets Mouton, Loup, Chasseur
Les moutons survivent en mangeant des cases d'herbe
Les loups surivent en mangeant des moutons
Les chasseurs ne meurent pas et chassent les loups 
Le jeu s'arrête lorsque le nombre de moutons a atteint un certain seuil ou l'horloge s'arrête
'''

#DESCRIPTION DES CLASSES

-Classe Monde:  
Attributs :   
    dimension = taille de la carte carrée (ex : 30 x 30)  
    duree_repousse = nombre de tours pour que de l'herbe pousse   
Méthodes :   
    herbePousse(): fait pousser de l'herbe, représentée par -1 en 1er indice. Incrémente de 1 la case pas d'herbe
    herbeMangee(): 0 en 1er indice  
    nbHerbe() : entier pour nombre d'herbes total sur la carte  
    CoefCarte(x,y): renvoie la case (sous forme de liste) à l'indice x,y  
    getCarte(): renvoie toute la carte, en liste de listes  

-Classe Mouton:  
Attributs :  
    position_x = correspond à ligne sur la carte  
    position_y = correspond à la colonne sur la carte  
    gain_nourriture = entier pour gain d'énergie par consommation d'un carré d'herbe  
    taux_reproduction = entier pour pourcentage de chance de reproduction (repoduction assexuée ici, c'est à dire tout seul)  
    carte = classe Monde à laquelle il appartient  
    energie = entier aléatoire   
    numMouton = entier servant comme clé unique à chaque instance d'objet  
Méthodes :  
    variationEnergie(): perd 1 d'énergie si case n'est pas herbue, sinon ajoute gain_nourriture   
    getcarte(): affiche la carte  
    deplacement(): se déplace aléatoirement dans les 8 cases adjacentes => x,y varient de -1 à 1  
    place_mouton() : insère l'instance sur les coordonnées en attribut  

-Classe Loup:  
Attributs :  
    position_x = correspond à ligne sur la carte  
    position_y = correspond à la colonne sur la carte  
    gain_nourriture = entier pour gain d'énergie par consommation d'un carré d'herbe  
    taux_reproduction = entier pour pourcentage de chance de reproduction (repoduction assexuée ici, c'est à dire tout seul)  
    carte = classe Monde à laquelle il appartient  
    energie = entier aléatoire   
    numMouton = entier servant comme clé unique à chaque instance d'objet  
Méthodes :  
    variationEnergie(): perd 1 d'énergie si case ne contient pas de mouton, sinon ajoute gain_nourriture   
    getcarte(): affiche la carte  
    deplacement(): se déplace aléatoirement dans les 8 cases adjacentes => x,y varient de -1 à 1  
    place_loup() : insère l'instance sur les coordonnées en attribut  
    trouver_mouton(): recherche de mouton dans les 8 cases adjacentes, renvoie numMouton  

-Classe Chasseur:  
Attributs:  
    position_x = ligne sur la carte  
    position_y = colonne sur la carte  
    carte = classe Monde  
    numChasseur = entier servant comme clé unique à chaque instance d'objet  
Méthodes:   
    deplacement(): se déplace aléatoirement dans les 8 cases adjacentes => x,y varient de -1 à 1  
    place_chasseur() : insère l'instance sur les coordonnées x, y en attribut  
    trouver_loup(): recherche de loup dans les 8 cases adjacentes, renvoie numLoup  

-Classe Simulation :

    Lire code déjà spécifié dans la fonction


FICHIER code.py

    Affiche un graphique montrant le nombre de moutons, d'herbe et de loups en fonction du temps passé

FICHIER getMap.py

    AFfiche une interface graphique représentant la carte Monde à la fin de la simulation