from classeMonde import Monde
from classeMouton import Mouton
from classeChasseur import Chasseur
from classeLoup import Loup
import re
from random import randint


class Simulation:
    def __init__(self,nombre_moutons, nombre_moutons_max,fin_du_monde, monde):
        self.monde = monde #héritance de classe etrangere
        self.dimension = self.monde.dimension #attribut de classe Monde
        self.nombre_moutons = nombre_moutons #nombre moutons à initialisation, puis devient le numéro identifiant chaque mouton
        self.nombre_loups = self.nombre_moutons // 2
        self.nombre_chasseurs = self.dimension // 5 #ratio de 1/5 chasseur par rapport à la dimension du monde
        self.fin_du_monde = fin_du_monde
        
        self.horloge = 0
        self.loups = [Loup(randint(0,self.dimension-1), randint(0,self.dimension-1), self.monde.carte, k) for k in range(self.nombre_loups)]
        self.moutons = [Mouton(randint(0,self.dimension-1), randint(0,self.dimension-1), self.monde.carte, i) for i in range(self.nombre_moutons)]
        self.chasseurs = [Chasseur(randint(0,self.dimension-1), randint(0,self.dimension-1), self.monde.carte, i) for i in range(self.nombre_chasseurs)]
        self.nombre_moutons_max = nombre_moutons_max
        self.resultats_herbe = []
        self.resultats_moutons = []
        self.resultats_loups = []

    def get_carte(self):
        print(self.monde.carte)
    def getmouton(self):
       print([i for i in self.moutons])

    def simMouton(self):
        for i in self.moutons :
            i.place_mouton()
        for j in self.loups:
            j.place_loup()
        for k in self.chasseurs:
            k.place_chasseur()
        while self.horloge < self.fin_du_monde : 
            if self.moutons == []:
                print('moutons extincts')
                break
            elif len(self.moutons) > self.nombre_moutons_max:
                print('moutons ont conquis le monde')
                break
            self.resultats_moutons.append(len(self.moutons))
            self.resultats_loups.append(len(self.loups))
            self.horloge += 1
            self.resultats_herbe.append(self.monde.nbHerbe())
            self.monde.herbePousse()


            for i in self.loups:
                i.variationEnergie()
                '''Loup meurt si energie nulle / Le retire de la liste des loups et de la carte'''
                if i.energie == 0:
                    self.loups.remove(i)
                    self.monde.carte[i.position_y][i.position_x].remove('Loup'+str(i.numLoup))
                    continue

                '''LOUP TUE MOUTON SI SUR LA MEME CASE'''
                pattern = r'^Mouton'
                for g in self.monde.carte[i.position_y][i.position_x]:
                    if re.match(pattern, str(g)): #Recherche de str Mouton dans la case          
                        self.monde.carte[i.position_y][i.position_x].remove(g) 
                        mouton_a_retirer = [l for l in self.moutons if l.numMouton == int(g[6:])] #Identification mouton dans self.moutons grâce à numéro
                        try:
                            self.moutons.remove(mouton_a_retirer[0]) # Ne retire qu'UN SEUL mouton de la case
                        except:
                            print('Aucune correspondance dans Moutons')
                        break #s'arrete quand un mouton trouvé
                '''Reproduction Loup'''
                apparait_ou_non = randint(1, 100)
                if apparait_ou_non <= i.taux_reproduction :
                    self.nombre_loups += 1
                    self.loups.append(Loup(i.position_x, i.position_y, self.monde.carte, self.nombre_loups))
                    self.monde.carte[i.position_y][i.position_x].append('Loup'+str(self.nombre_loups))

                '''Déplacement Loup''' 
                self.monde.carte[i.position_y][i.position_x].remove('Loup'+str(i.numLoup))
                if i.trouver_mouton() != False:
                    mouton_a_tuer = [mouton for mouton in self.moutons if mouton.numMouton == i.trouver_mouton()] #renvoie une liste d'un élément : type Mouton
                    mouton_a_tuer = mouton_a_tuer[0]
                    i.position_y = mouton_a_tuer.position_y
                    i.position_x = mouton_a_tuer.position_x
                else: #si aucun mouton autour, se déplace dans une case aléatoire adjacente
                    i.deplacement()
                i.place_loup()

            for i in self.moutons:
                i.variationEnergie()
                '''Mouton meurt si energie nulle / Le retire de la liste des moutons et de la carte'''
                if i.energie == 0:
                    self.moutons.remove(i)
                    self.monde.carte[i.position_y][i.position_x].remove('Mouton'+str(i.numMouton))
                    continue #retour en début de boucle for sans exécuter code en bas

                '''Si mouton sur une case d'herbe, herbe mangee'''
                if self.monde.carte[i.position_y][i.position_x][0] == -1:
                    self.monde.herbeMangee(i.position_y, i.position_x)
                
                '''Reproduction Mouton'''
                apparait_ou_non = randint(1, 100)
                if apparait_ou_non <= i.taux_reproduction :
                    self.nombre_moutons += 1
                    self.moutons.append(Mouton(i.position_x, i.position_y, self.monde.carte, self.nombre_moutons))
                    self.monde.carte[i.position_y][i.position_x].append('Mouton'+str(self.nombre_moutons))

                '''Déplacement Mouton'''
                self.monde.carte[i.position_y][i.position_x].remove('Mouton'+str(i.numMouton))
                i.deplacement()
                i.place_mouton()

            
            for i in self.chasseurs:

                ''' Tuer loup sur même case'''
                pattern = r'^Loup'
                for g in self.monde.carte[i.position_y][i.position_x]:
                    if re.match(pattern, str(g)): #Recherche de str Loup dans la case          
                        self.monde.carte[i.position_y][i.position_x].remove(g) 
                        loup_a_retirer = [l for l in self.loups if l.numLoup == int(g[4:])] #Identification loup dans self.moutons grâce à numéro
                        try:
                            self.loups.remove(loup_a_retirer[0]) # Ne retire qu'UN SEUL loup de la case
                        except:
                            print('Aucune correspondance dans Loups')
                        break #s'arrete quand un loup trouvé
                
                ''' Se déplacer vers loup si dans les cases adjacentes'''
                self.monde.carte[i.position_y][i.position_x].remove('Chasseur'+str(i.numChasseur))
                if i.trouver_loup() != False:
                    loup_a_tuer = [loup for loup in self.loups if loup.numLoup == i.trouver_loup()] #renvoie une liste d'un élément : type Loup
                    loup_a_tuer = loup_a_tuer[0]
                    ''' attributs x, y du chasseur deviennent celui du loup = meme case '''
                    i.position_y = loup_a_tuer.position_y
                    i.position_x = loup_a_tuer.position_x
                else: #si aucun loup autour, se déplace dans une case aléatoire adjacente
                    i.deplacement()
                i.place_chasseur()

        return self.resultats_herbe, self.resultats_moutons, self.resultats_loups, self.horloge, self.monde.carte
